package com.diogostein.agendadieese.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.diogostein.agendadieese.R;
import com.diogostein.agendadieese.daos.ContactDAO;
import com.diogostein.agendadieese.helpers.DateHelper;
import com.diogostein.agendadieese.helpers.TextHelper;
import com.diogostein.agendadieese.models.ContactModel;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Diogo on 08/05/2015.
 */
public class NewContactDialogFragment extends DialogFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public static final String EXTRA_DIALOG_CONTACT_FORM = "FormContactDialogFragment";
    private static final int ACTION_TYPE_NEW = 0;
    private static final int ACTION_TYPE_EDIT = 1;
    private static final int REQUEST_CODE_NAME = 0;
    private static final int REQUEST_CODE_COMPANY = 1;
    private static final String TAG_DATEPICKER = "datepicker";

    private ContactModel mContact;
    private Activity mActivity;
    private Intent mItVoiceRecognizer;
    private Resources mRes;

    private Calendar mCalendar;
    private DatePickerDialog mDatePickerDialog;

    private ImageButton mBtnClose;
    private ImageButton mBtnClearBirth;
    private ImageButton mBtnSpeakName;
    private ImageButton mBtnSpeakCompany;
    private Button mBtnSave;
    private EditText mEditName;
    private EditText mEditBirth;
    private EditText mEditCompany;
    private EditText mEditEmail;
    private EditText mEditPhoneCel;
    private EditText mEditPhoneRes;
    private EditText mEditPhoneCom;

    private int mActionType;
    private String mBirth;
    private String mBirthFormatted;
    private boolean mDeviceHasVoiceRecognizer;

    private OnNewContactDialogResultListener mListener;

    // New Contact
    public static NewContactDialogFragment newInstance(OnNewContactDialogResultListener listener) {
        NewContactDialogFragment df = new NewContactDialogFragment();
        df.setActionType(ACTION_TYPE_NEW);
        df.setOnNewContactDialogResultListener(listener);
        return df;
    }

    // Edit Contact
    public static NewContactDialogFragment newInstance(OnNewContactDialogResultListener listener, ContactModel contact) {
        NewContactDialogFragment df = new NewContactDialogFragment();
        df.setActionType(ACTION_TYPE_EDIT);
        df.setOnNewContactDialogResultListener(listener);
        df.setContact(contact);
        return df;
    }

    private void setActionType(int type) {
        mActionType = type;
    }

    private void setOnNewContactDialogResultListener(OnNewContactDialogResultListener listener) {
        mListener = listener;
    }

    private void setContact(ContactModel contact) {
        mContact = contact;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.NewContactDialog);
        mActivity = getActivity();
        mRes = getResources();
        mBirth = "";
        mBirthFormatted = mRes.getString(R.string.date_pattern_abr_month);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_contact, container, false);

        setTitle(v);
        prepareVoiceRecognizer();
        prepareDatePicker();
        loadViews(v);
        fillFields();

        // Avoid closing datepicker rebuilding fragment
        if(savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog) getActivity().getSupportFragmentManager().findFragmentByTag(TAG_DATEPICKER);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
            }
        }

        return v;
    }

    private void loadViews(View v) {
        mBtnClose = (ImageButton) v.findViewById(R.id.button_dismiss);
        mBtnClearBirth = (ImageButton) v.findViewById(R.id.button_clear_birth);
        mBtnSpeakName = (ImageButton) v.findViewById(R.id.button_speak_name);
        mBtnSpeakCompany = (ImageButton) v.findViewById(R.id.button_speak_company);
        mBtnSave = (Button) v.findViewById(R.id.button_save);
        mEditName = (EditText) v.findViewById(R.id.edit_name);
        mEditBirth = (EditText) v.findViewById(R.id.edit_birth);
        mEditCompany = (EditText) v.findViewById(R.id.edit_company);
        mEditEmail = (EditText) v.findViewById(R.id.edit_email);
        mEditPhoneCel = (EditText) v.findViewById(R.id.edit_phone_cel);
        mEditPhoneRes = (EditText) v.findViewById(R.id.edit_phone_res);
        mEditPhoneCom = (EditText) v.findViewById(R.id.edit_phone_com);

        mBtnClose.setOnClickListener(this);
        mEditBirth.setOnClickListener(this);
        mBtnClearBirth.setOnClickListener(this);
        mBtnSpeakName.setOnClickListener(this);
        mBtnSpeakCompany.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
    }

    private void fillFields() {
        if (mActionType == ACTION_TYPE_EDIT && mContact != null) {
            mEditName.setText(mContact.name);
            mEditBirth.setText(DateHelper.format(mContact.birth, mBirthFormatted));
            mEditCompany.setText(mContact.company);
            mEditEmail.setText(mContact.email);
            mEditPhoneCel.setText(mContact.phoneCel);
            mEditPhoneRes.setText(mContact.phoneRes);
            mEditPhoneCom.setText(mContact.phoneCom);
            mBirth = mContact.birth;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if(d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.edit_birth: showDatePicker(); break;
            case R.id.button_clear_birth: clearBirth(); break;
            case R.id.button_speak_name: startVoiceRecognizer(REQUEST_CODE_NAME); break;
            case R.id.button_speak_company: startVoiceRecognizer(REQUEST_CODE_COMPANY); break;
            case R.id.button_dismiss: dismiss(); break;
            case R.id.button_save: save();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        month += 1;
        String fDate = String.format("%s-%s-%s", year, month, day);
        mEditBirth.setText(DateHelper.format(fDate, mBirthFormatted));
        mBirth = DateHelper.format(fDate, "yyyy-M-d", "yyyy-MM-dd");
    }

    private void save() {
        String name = mEditName.getText().toString().trim();
        String birth = mBirth;
        String company = mEditCompany.getText().toString().trim();
        String email = mEditEmail.getText().toString().trim().toLowerCase();
        String phoneCel = mEditPhoneCel.getText().toString().trim();
        String phoneRes = mEditPhoneRes.getText().toString().trim();
        String phoneCom = mEditPhoneCom.getText().toString().trim();

        if(!TextUtils.isEmpty(email) && !TextHelper.isValidEmail(email)) {
            mEditEmail.requestFocus();
            Toast.makeText(mActivity, mRes.getString(R.string.toast_invalid_email), Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(birth) || (TextUtils.isEmpty(phoneCel)
                && TextUtils.isEmpty(phoneRes) && TextUtils.isEmpty(phoneCom))) {
            Toast.makeText(mActivity, mRes.getString(R.string.toast_validate_new_contact), Toast.LENGTH_LONG).show();
            return;
        }

        ContactDAO contactDAO = new ContactDAO(mActivity);
        try {
            contactDAO.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        long result = 0;
        ContactModel contact = null;

        switch(mActionType) {
            case ACTION_TYPE_NEW:
                contact = new ContactModel(name, birth, company, email, phoneCel, phoneRes, phoneCom);
                result = contactDAO.create(contact);
                break;
            case ACTION_TYPE_EDIT:
                contact = new ContactModel(mContact.id, name, birth, company, email, phoneCel, phoneRes, phoneCom);
                result = contactDAO.update(contact);
        }

        if(result > 0) {
            if(mActionType == ACTION_TYPE_NEW) contact.id = result;
            mListener.onNewContactDialogResult(contact);
            contactDAO.close();
            dismiss();
            Toast.makeText(mActivity, mRes.getString(R.string.toast_save_success), Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mActivity, mRes.getString(R.string.toast_error_on_save), Toast.LENGTH_SHORT).show();
    }

    private void startVoiceRecognizer(int requestCode) {
        if(mDeviceHasVoiceRecognizer)
            startActivityForResult(mItVoiceRecognizer, requestCode);
        else {
            Toast.makeText(mActivity, mRes.getString(R.string.toast_no_vc_supported), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            String result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0);
            switch(requestCode) {
                case REQUEST_CODE_NAME: mEditName.setText(TextHelper.capitalize(result)); break;
                case REQUEST_CODE_COMPANY: mEditCompany.setText(TextHelper.capitalize(result)); break;
            }
        }
    }

    private void prepareVoiceRecognizer() {
        PackageManager pm = mActivity.getPackageManager();
        mItVoiceRecognizer = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        List<ResolveInfo> activities = pm.queryIntentActivities(mItVoiceRecognizer, 0);
        if(activities.size() != 0) {
            mItVoiceRecognizer.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            mItVoiceRecognizer.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        }
        mDeviceHasVoiceRecognizer = activities.size() != 0;
    }

    private void prepareDatePicker() {
        mCalendar = Calendar.getInstance();
        mDatePickerDialog = DatePickerDialog.newInstance(
                this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH), true);

        mDatePickerDialog.setVibrate(true);
        mDatePickerDialog.setYearRange(1910, mCalendar.get(Calendar.YEAR));
        mDatePickerDialog.setCloseOnSingleTapDay(false);
    }

    private void showDatePicker() {
        if(!mDatePickerDialog.isVisible())
            mDatePickerDialog.show(getActivity().getSupportFragmentManager(), TAG_DATEPICKER);
    }

    private void clearBirth() {
        mEditBirth.setText("");
        mBirth = "";
    }

    private void setTitle(View v) {
        TextView tvTitle = (TextView) v.findViewById(R.id.text_title);
        tvTitle.setText(mActionType == ACTION_TYPE_NEW
                ? getResources().getString(R.string.title_dialog_new_contact)
                : getResources().getString(R.string.title_dialog_edit_contact));
    }

    public interface OnNewContactDialogResultListener {
        void onNewContactDialogResult(ContactModel contact);
    }
}
