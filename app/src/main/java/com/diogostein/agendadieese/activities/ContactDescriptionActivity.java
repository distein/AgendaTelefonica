package com.diogostein.agendadieese.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.diogostein.agendadieese.R;
import com.diogostein.agendadieese.daos.ContactDAO;
import com.diogostein.agendadieese.fragments.NewContactDialogFragment;
import com.diogostein.agendadieese.helpers.AlertDialogHelper;
import com.diogostein.agendadieese.helpers.DateHelper;
import com.diogostein.agendadieese.models.ContactModel;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;

import java.sql.SQLException;
import java.util.List;

public class ContactDescriptionActivity extends ActionBarActivity
        implements NewContactDialogFragment.OnNewContactDialogResultListener, View.OnClickListener,
        ObservableScrollViewCallbacks {

    public static final String EXTRA_CONTACT = "contact";

    private ContactModel mContact;
    private Intent mItDialer;

    private Toolbar mToolbar;
    private TextView mTxtName;
    private TextView mTxtPhoneCel;
    private TextView mTxtPhoneRes;
    private TextView mTxtPhoneCom;
    private TextView mTxtBirth;
    private TextView mTxtCompany;
    private TextView mTxtEmail;
    private LinearLayout mContainerPhoneCel;
    private LinearLayout mContainerPhoneRes;
    private LinearLayout mContainerPhoneCom;
    private LinearLayout mContainerCompany;
    private LinearLayout mContainerEmail;
    private ImageButton mBtnCallPhoneCel;
    private ImageButton mBtnCallPhoneRes;
    private ImageButton mBtnCallPhoneCom;
    private ObservableScrollView mScrollView;
    private FrameLayout mFrameBackground;

    private boolean mDeviceHasDialer;
    private int mParallaxImageHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_description);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContact = getIntent().getParcelableExtra(EXTRA_CONTACT);
        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
        loadViews();
        prepareDialAction();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
    }

    private void loadViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTxtName = (TextView) findViewById(R.id.text_name);
        mTxtPhoneCel = (TextView) findViewById(R.id.text_phone_cel);
        mTxtPhoneRes = (TextView) findViewById(R.id.text_phone_res);
        mTxtPhoneCom = (TextView) findViewById(R.id.text_phone_com);
        mTxtBirth = (TextView) findViewById(R.id.text_birth);
        mTxtCompany = (TextView) findViewById(R.id.text_company);
        mTxtEmail = (TextView) findViewById(R.id.text_email);
        mContainerPhoneCel = (LinearLayout) findViewById(R.id.container_phone_cel);
        mContainerPhoneRes = (LinearLayout) findViewById(R.id.container_phone_res);
        mContainerPhoneCom = (LinearLayout) findViewById(R.id.container_phone_com);
        mContainerCompany = (LinearLayout) findViewById(R.id.container_company);
        mContainerEmail = (LinearLayout) findViewById(R.id.container_email);
        mBtnCallPhoneCel = (ImageButton) findViewById(R.id.button_call_cel);
        mBtnCallPhoneRes = (ImageButton) findViewById(R.id.button_call_res);
        mBtnCallPhoneCom = (ImageButton) findViewById(R.id.button_call_com);
        mScrollView = (ObservableScrollView) findViewById(R.id.scrollview);
        mFrameBackground = (FrameLayout) findViewById(R.id.frame_background);
        setUpViews();
    }

    private void setUpViews() {
        setTitle(mContact.name);

        mTxtName.setText(mContact.name);
        mTxtBirth.setText(DateHelper.format(mContact.birth, getResources().getString(R.string.date_pattern_complete)));
        showIfNotEmpty(mContact.phoneCel, mTxtPhoneCel, mContainerPhoneCel);
        showIfNotEmpty(mContact.phoneRes, mTxtPhoneRes, mContainerPhoneRes);
        showIfNotEmpty(mContact.phoneCom, mTxtPhoneCom, mContainerPhoneCom);
        showIfNotEmpty(mContact.company, mTxtCompany, mContainerCompany);
        showIfNotEmpty(mContact.email, mTxtEmail, mContainerEmail);
        mBtnCallPhoneCel.setOnClickListener(this);
        mBtnCallPhoneRes.setOnClickListener(this);
        mBtnCallPhoneCom.setOnClickListener(this);
        mScrollView.setScrollViewCallbacks(this);

        mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.colorPrimary)));
        mToolbar.setTitleTextColor(Color.argb(0, 255, 255, 255));
    }

    private void showIfNotEmpty(String s, TextView textView, LinearLayout container) {
        if(TextUtils.isEmpty(s)) {
            if(container.getVisibility() == View.VISIBLE)
                container.setVisibility(View.GONE);
        } else {
            if(container.getVisibility() == View.GONE)
                container.setVisibility(View.VISIBLE);
            textView.setText(s);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button_call_cel: callTo(mContact.phoneCel); break;
            case R.id.button_call_res: callTo(mContact.phoneRes); break;
            case R.id.button_call_com: callTo(mContact.phoneCom); break;
        }
    }

    private void callTo(String number) {
        if(mDeviceHasDialer) {
            number = String.format("tel:%s", number);
            mItDialer.setData(Uri.parse(number));
            startActivity(mItDialer);
        } else
            Toast.makeText(this, getResources().getString(R.string.toast_no_dialer_supported), Toast.LENGTH_SHORT).show();
    }

    private void prepareDialAction() {
        PackageManager pm = getPackageManager();
        mItDialer = new Intent(Intent.ACTION_DIAL);
        List<ResolveInfo> activities = pm.queryIntentActivities(mItDialer, 0);
        mDeviceHasDialer = activities.size() != 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact_description, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_edit) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            DialogFragment newFragment = NewContactDialogFragment.newInstance(this, mContact);
            newFragment.show(ft, NewContactDialogFragment.EXTRA_DIALOG_CONTACT_FORM);
            return true;
        } else if(id == R.id.action_delete) {
            String title = getResources().getString(R.string.alert_dialog_delete_title);
            String message = getResources().getString(R.string.alert_dialog_delete_message);
            AlertDialogHelper alert = new AlertDialogHelper(this, title, message);
            alert.setOnAlertOptionListener(new AlertDialogHelper.OnAlertOptionListener() {
                @Override
                public void onAlertOptionClick(Context context, int option) {
                    if(option == AlertDialogHelper.OPTION_YES) {
                        ContactDAO contactDAO = new ContactDAO(context);
                        try {
                            contactDAO.open();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if(contactDAO.delete(mContact.id)) finish();
                        else Toast.makeText(context, getResources().getString(R.string.toast_error_on_delete), Toast.LENGTH_SHORT).show();
                        contactDAO.close();
                    }
                }
            });
            alert.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNewContactDialogResult(ContactModel contact) {
        if(contact != null) {
            mContact = contact;
            setUpViews();
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        boolean reachedBottom = mScrollView.getChildAt(0).getBottom()-(mScrollView.getHeight()+scrollY) == 0;
        int toolbarColor = getResources().getColor(R.color.colorPrimary);
        int titleColor = getResources().getColor(R.color.colorTextLight);
        float alpha = Math.min(1, !reachedBottom ? (float) scrollY / mParallaxImageHeight : 1);

        ViewHelper.setTranslationY(mFrameBackground, scrollY / 2);

        mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, toolbarColor));
        mToolbar.setTitleTextColor(ScrollUtils.getColorWithAlpha(alpha, titleColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
}
