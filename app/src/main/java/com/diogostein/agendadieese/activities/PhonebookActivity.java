package com.diogostein.agendadieese.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.diogostein.agendadieese.R;
import com.diogostein.agendadieese.adapters.ContactAdapter;
import com.diogostein.agendadieese.daos.ContactDAO;
import com.diogostein.agendadieese.fragments.NewContactDialogFragment;
import com.diogostein.agendadieese.helpers.AlertDialogHelper;
import com.diogostein.agendadieese.helpers.HidingScrollListener;
import com.diogostein.agendadieese.models.ContactModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diogo on 08/05/2015.
 */
public class PhonebookActivity extends BaseActivity implements ContactAdapter.OnContactItemListener,
        NewContactDialogFragment.OnNewContactDialogResultListener, View.OnClickListener, ActionMode.Callback {

    private LoadFromDBTask mTask;
    private ContactDAO mContactDAO;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<ContactModel> mContacts;
    private ContactAdapter mAdapter;

    private LinearLayout mContainerEmpty;
    private ProgressBar mProgress;
    private ImageButton mBtnNewContact;
    private SearchView mSearchView;

    private ActionMode mActionMode;
    private MenuItem mSearchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phonebook);

        mContactDAO = new ContactDAO(this);

        mContainerEmpty = (LinearLayout) findViewById(R.id.container_empty);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ContactAdapter(this, mContacts, this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnScrollListener(new HidingScrollListener() {
            @Override public void onHide() { hideNewContactButton(); }
            @Override public void onShow() { showNewContactButton(); }
        });

        mBtnNewContact = (ImageButton) findViewById(R.id.button_new_contact);
        mBtnNewContact.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        try {
            mContactDAO.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mTask = new LoadFromDBTask();
        mTask.execute();
        super.onResume();
    }

    @Override
    protected void onPause() {
        collapseSearchItem();
        mContactDAO.close();
        super.onPause();
    }

    @Override
    public void onContactItemClick(ContactModel contact, int position) {
        if(mActionMode == null) {
            Intent it = new Intent(this, ContactDescriptionActivity.class);
            it.putExtra(ContactDescriptionActivity.EXTRA_CONTACT, contact);
            startActivity(it);
        } else {
            mAdapter.toggleSelection(position);
            String actionModeTitle = getResources().getString(R.string.actionmode_count_selected, mAdapter.getSelectedItemCount());
            mActionMode.setTitle(actionModeTitle);
            if(mAdapter.getSelectedItemCount() == 0) mActionMode.finish();
        }
    }

    @Override
    public boolean onContactLongItemClick(ContactModel contact, int position) {
        if(mActionMode == null && !mSearchItem.isActionViewExpanded()) {
            mActionMode = startSupportActionMode(this);
            mAdapter.toggleSelection(position);
            String actionModeTitle = getResources().getString(R.string.actionmode_count_selected, mAdapter.getSelectedItemCount());
            mActionMode.setTitle(actionModeTitle);
        }
        return true;
    }

    @Override
    public void onNewContactDialogResult(ContactModel contact) {
        mAdapter.add(contact);
        checkIfIsEmpty();
        showNewContactButton();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_new_contact) {
            if(mActionMode != null) mActionMode.finish();
            collapseSearchItem();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            DialogFragment newFragment = NewContactDialogFragment.newInstance(this);
            newFragment.show(ft, NewContactDialogFragment.EXTRA_DIALOG_CONTACT_FORM);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_phonebook, menu);
        mSearchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchItem);
        mSearchView.setQueryHint(getString(R.string.hint_search_contact));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                mAdapter.getFilter().filter(s);
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(mSearchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actionmode_phonebook, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(final ActionMode actionMode, MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.action_delete) {

            String title = getResources().getString(R.string.alert_dialog_delete_title);
            String message = getResources().getString(R.string.alert_dialog_delete_message);
            AlertDialogHelper alert = new AlertDialogHelper(this, title, message);
            alert.setOnAlertOptionListener(new AlertDialogHelper.OnAlertOptionListener() {
                @Override
                public void onAlertOptionClick(Context context, int option) {
                    if(option == AlertDialogHelper.OPTION_YES) {
                        List<Integer> selectedItemPositions =  mAdapter.getSelectedItems();
                        List<String> idsList = new ArrayList<>();

                        for(int i = 0; i < selectedItemPositions.size(); i++)
                            idsList.add(String.valueOf(mAdapter.getItem(selectedItemPositions.get(i)).id));

                        String[] ids = new String[idsList.size()];
                        for(int i = 0; i < ids.length; i++) ids[i] = idsList.get(i);

                        if(mContactDAO.groupDelete(ids)) {
                            List<Integer> newSelectedItemPositions =  mAdapter.getSelectedItems();
                            for(int i = newSelectedItemPositions.size() - 1; i >= 0; i--) {
                                mAdapter.removeItem(newSelectedItemPositions.get(i));
                            }
                        }
                        actionMode.finish();
                        checkIfIsEmpty();
                        showNewContactButton();
                    }
                }
            });
            alert.show();
        }
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        mActionMode = null;
        mAdapter.clearSelections();
    }

    private void collapseSearchItem() {
        if(mSearchItem != null) {
            if(mSearchItem.isActionViewExpanded())
                mSearchItem.collapseActionView();
        }
    }

    private void checkIfIsEmpty() {
        if(mAdapter.isEmpty()) {
            if(mContainerEmpty.getVisibility() == View.VISIBLE) mContainerEmpty.setVisibility(View.GONE);
            if(mRecyclerView.getVisibility() == View.GONE) mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            if(mRecyclerView.getVisibility() == View.VISIBLE) mRecyclerView.setVisibility(View.GONE);
            if(mContainerEmpty.getVisibility() == View.GONE) mContainerEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void showNewContactButton() {
        mBtnNewContact.animate()
                .translationY(0)
                .setInterpolator(new AccelerateInterpolator(1));
    }

    private void hideNewContactButton() {
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mBtnNewContact.getLayoutParams();
        mBtnNewContact.animate()
                .translationY(mBtnNewContact.getHeight() + lp.bottomMargin)
                .setInterpolator(new AccelerateInterpolator(1));
    }

    private void showProgress(boolean show) {
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mContainerEmpty.setVisibility(show ? View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    class LoadFromDBTask extends AsyncTask<Void, Void, List<ContactModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected List<ContactModel> doInBackground(Void... params) {
            return mContactDAO.getAllContacts();
        }

        @Override
        protected void onPostExecute(List<ContactModel> contacts) {
            super.onPostExecute(contacts);
            showProgress(false);
            mContacts = contacts;
            mAdapter.reloadData(mContacts);
            checkIfIsEmpty();
            showNewContactButton();
        }
    }
}
