package com.diogostein.agendadieese.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diogostein.agendadieese.R;
import com.diogostein.agendadieese.models.ContactModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Diogo on 08/05/2015.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    private Context mContext;
    private List<ContactModel> mContacts;
    private List<ContactModel> mFilter;
    private OnContactItemListener mListener;

    private SparseBooleanArray mSelectedItems;

    public ContactAdapter(Context context, List<ContactModel> contacts, OnContactItemListener listener) {
        mContext = context;
        mContacts = contacts;
        mListener = listener;
        mSelectedItems = new SparseBooleanArray();
        sortContactsByName();
    }

    @Override
    public int getItemCount() { return mContacts != null ? mContacts.size() : 0; }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.adapter_contact, parent, false);
        ContactAdapter.ContactViewHolder vh = new ContactAdapter.ContactViewHolder(v);
        TextView tvName = (TextView) v.findViewById(R.id.text_name);
        tvName.setTag(vh);
        tvName.setOnClickListener(this);
        tvName.setOnLongClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        ContactModel contact = mContacts.get(position);

        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)holder.container.getLayoutParams();
        marginLayoutParams.setMargins(0, position == 0 ? 16 : 0, 0, 0);

        char letter = contact.name.toUpperCase().charAt(0);
        holder.tvLetter.setText(String.valueOf(letter));
        holder.tvName.setText(contact.name);

        if(position < getItemCount() - 1) {
            char nextLetter = mContacts.get(position + 1).name.toUpperCase().charAt(0);
            if(letter != nextLetter) {
                holder.divider.setVisibility(View.VISIBLE);
            } else {
                holder.divider.setVisibility(View.GONE);
            }
        }

        holder.tvName.setActivated(mSelectedItems.get(position, false));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.text_name: fireOnContactItemClick(v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch(v.getId()) {
            case R.id.text_name: return fireOnContactLongItemClick(v);
        }
        return false;
    }

    private void fireOnContactItemClick(View v) {
        if(mListener != null){
            ContactViewHolder vh = (ContactViewHolder) v.getTag();
            int position = vh.getPosition();
            mListener.onContactItemClick(mContacts.get(position), position);
        }
    }

    private boolean fireOnContactLongItemClick(View v) {
        if (mListener != null) {
            ContactViewHolder vh = (ContactViewHolder) v.getTag();
            int position = vh.getPosition();
            return mListener.onContactLongItemClick(mContacts.get(position), position);
        }
        return false;
    }

    public ContactModel getItem(int position) {
        return mContacts.get(position);
    }

    public boolean isEmpty() {
        return getItemCount() > 0;
    }

    public void add(ContactModel contact) {
        mContacts.add(contact);
        mFilter = null;
        sortContactsByName();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mContacts.remove(position);
        mFilter = null;
        sortContactsByName();
        notifyDataSetChanged();
    }

    public void reloadData(List<ContactModel> contacts) {
        mContacts = contacts;
        mFilter = null;
        sortContactsByName();
        notifyDataSetChanged();
    }

    private void sortContactsByName() {
        if(mContacts != null) {
            Collections.sort(mContacts);
        }
    }

    public void toggleSelection(int position) {
        if (mSelectedItems.get(position, false)) {
            mSelectedItems.delete(position);
        } else {
            mSelectedItems.put(position, true);
        }
        notifyItemChanged(position);
    }

    public void clearSelections() {
        mSelectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return mSelectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(mSelectedItems.size());
        for (int i = 0; i < mSelectedItems.size(); i++) {
            items.add(mSelectedItems.keyAt(i));
        }
        return items;
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults fr = new FilterResults();
                final List<ContactModel> results = new ArrayList<>();
                if(mFilter == null) mFilter = new ArrayList<>(mContacts);
                if(constraint != null) {
                    if (mFilter.size() > 0) {
                        for (final ContactModel g : mFilter) {
                            if (g.name.toLowerCase().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    fr.count = results.size();
                    fr.values = results;
                }
                return fr;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mContacts = (ArrayList<ContactModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnContactItemListener {
        void onContactItemClick(ContactModel contact, int position);
        boolean onContactLongItemClick(ContactModel contact, int position);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout container;
        public TextView tvLetter;
        public TextView tvName;
        public View divider;

        public ContactViewHolder(View parent) {
            super(parent);
            container = (LinearLayout) parent.findViewById(R.id.container);
            divider = parent.findViewById(R.id.divider);
            tvLetter = (TextView) parent.findViewById(R.id.text_letter);
            tvName = (TextView) parent.findViewById(R.id.text_name);
        }
    }
}
