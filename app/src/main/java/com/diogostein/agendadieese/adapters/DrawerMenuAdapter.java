package com.diogostein.agendadieese.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diogostein.agendadieese.R;
import com.diogostein.agendadieese.models.DrawerItemModel;

import java.util.List;

/**
 * Created by Diogo on 08/05/2015.
 */
public class DrawerMenuAdapter extends ArrayAdapter<DrawerItemModel> {

    private Context mContext;
    private List<DrawerItemModel> mItems;
    private OnDrawerItemClickListener mListener;

    public DrawerMenuAdapter(Context context, int resource, List<DrawerItemModel> items, OnDrawerItemClickListener listener) {
        super(context, resource, items);
        mContext = context;
        mItems = items;
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public DrawerItemModel getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final DrawerItemModel drawerItem = mItems.get(position);
        ViewHolder holder;

        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_navdrawer, parent, false);
            holder = new ViewHolder();
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.icon.setImageDrawable(drawerItem.icon);
        holder.title.setText(drawerItem.title);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDrawerItemClick(drawerItem);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        ImageView icon;
        TextView title;
    }

    public interface OnDrawerItemClickListener {
        void onDrawerItemClick(DrawerItemModel drawerItem);
    }
}
