package com.diogostein.agendadieese.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Diogo on 08/05/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    // DATABASE //
    public static final String DATABASE_NAME = "phonebook";
    public static final int DATABASE_VERSION = 1;

    // TABLE //
    public static final String TABLE_CONTACTS = "contacts";

    // COLUMNS //
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_BIRTH = "birth";
    public static final String COLUMN_COMPANY = "company";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PHONE_CEL = "phone_cel";
    public static final String COLUMN_PHONE_RES = "phone_res";
    public static final String COLUMN_PHONE_COM = "phone_com";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_UPDATED_AT = "updated_at";

    // CREATE TABLE //
    private static final String DATABASE_CREATE_CONTACTS = "CREATE TABLE "
            + TABLE_CONTACTS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NAME + " TEXT NOT NULL, "
            + COLUMN_BIRTH + " TEXT NOT NULL, "
            + COLUMN_COMPANY + " TEXT, "
            + COLUMN_EMAIL + " TEXT, "
            + COLUMN_PHONE_CEL + " TEXT, "
            + COLUMN_PHONE_RES + " TEXT, "
            + COLUMN_PHONE_COM + " TEXT, "
            + COLUMN_CREATED_AT + " TEXT NOT NULL, "
            + COLUMN_UPDATED_AT + " TEXT NOT NULL);";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_CONTACTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
    }
}
