package com.diogostein.agendadieese.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.diogostein.agendadieese.R;

/**
 * Created by Diogo on 11/05/2015.
 */
public final class AlertDialogHelper extends AlertDialog.Builder {

    public static final int OPTION_YES = 0;
    public static final int OPTION_NO = 1;

    private OnAlertOptionListener mListener;

    public AlertDialogHelper(final Context context, String title, String message) {
        super(context);
        setTitle(title);
        setMessage(message);
        setPositiveButton(context.getResources().getString(R.string.alert_dialog_option_yes),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onAlertOptionClick(context, OPTION_YES);
            }
        });
        setNegativeButton(context.getResources().getString(R.string.alert_dialog_option_no),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onAlertOptionClick(context, OPTION_NO);
            }
        });
        create();
    }

    public void setOnAlertOptionListener(OnAlertOptionListener listener) {
        mListener = listener;
    }

    public interface OnAlertOptionListener {
        void onAlertOptionClick(Context context, int option);
    }


}
