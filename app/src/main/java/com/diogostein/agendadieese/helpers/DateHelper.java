package com.diogostein.agendadieese.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Diogo on 08/05/2015.
 */
public final class DateHelper {

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private DateHelper() {}

    public static String getCurrentDate() {
        SimpleDateFormat df = new SimpleDateFormat(DATETIME_PATTERN, Locale.getDefault());
        return df.format(new Date());
    }

    public static String format(String date, String to) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(DATE_PATTERN);
        SimpleDateFormat toFormat = new SimpleDateFormat(to);
        return parseFormat(date, fromFormat, toFormat);
    }

    public static String format(String date, String from, String to) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(from);
        SimpleDateFormat toFormat = new SimpleDateFormat(to);
        return parseFormat(date, fromFormat, toFormat);
    }

    private static String parseFormat(String date, DateFormat fromFormat, DateFormat toFormat) {
        String fDate = date;
        try {
            fDate = toFormat.format(fromFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fDate;
    }
}
