package com.diogostein.agendadieese.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;

import com.diogostein.agendadieese.R;

/**
 * Created by Diogo on 12/05/2015.
 */
public final class AboutDialog extends AlertDialog.Builder {

    private AboutDialog(Context c) {
        super(c);
        Resources res = c.getResources();
        setTitle(res.getString(R.string.about_title));
        setMessage(res.getString(R.string.about_message));
        setNeutralButton(res.getString(R.string.about_button_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        create();
    }

    public static void showDialog(Context c) {
        new AboutDialog(c).show();
    }
}
