package com.diogostein.agendadieese.models;

import android.graphics.drawable.Drawable;

/**
 * Created by Diogo on 08/05/2015.
 */
public class DrawerItemModel {

    public int id;
    public String title;
    public Drawable icon;

    public DrawerItemModel(int id, String title, Drawable icon) {
        this.id = id;
        this.title = title;
        this.icon = icon;
    }

}
