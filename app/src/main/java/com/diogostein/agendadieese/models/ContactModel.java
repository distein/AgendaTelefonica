package com.diogostein.agendadieese.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Diogo on 08/05/2015.
 */
public class ContactModel implements Parcelable, Comparable<ContactModel> {

    public long id;
    public String name;
    public String birth;
    public String company;
    public String email;
    public String phoneCel;
    public String phoneRes;
    public String phoneCom;
    public String createdAt;
    public String updatedAt;

    // Get contacts
    public ContactModel(long id, String name, String birth, String company, String email,
                        String phoneCel, String phoneRes, String phoneCom, String createdAt, String updatedAt) {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.company = company;
        this.email = email;
        this.phoneCel = phoneCel;
        this.phoneRes = phoneRes;
        this.phoneCom = phoneCom;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    // Add new contact
    public ContactModel(String name, String birth, String company, String email, String phoneCel,
                        String phoneRes, String phoneCom) {
        this.name = name;
        this.birth = birth;
        this.company = company;
        this.email = email;
        this.phoneCel = phoneCel;
        this.phoneRes = phoneRes;
        this.phoneCom = phoneCom;
    }

    // Edit contact
    public ContactModel(long id, String name, String birth, String company, String email, String phoneCel,
                        String phoneRes, String phoneCom) {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.company = company;
        this.email = email;
        this.phoneCel = phoneCel;
        this.phoneRes = phoneRes;
        this.phoneCom = phoneCom;
    }

    private ContactModel(Parcel from) {
        id = from.readLong();
        name = from.readString();
        birth = from.readString();
        company = from.readString();
        email = from.readString();
        phoneCel = from.readString();
        phoneRes = from.readString();
        phoneCom = from.readString();
        createdAt = from.readString();
        updatedAt = from.readString();
    }

    @Override
    public int compareTo(ContactModel another) {
        return name.compareToIgnoreCase(another.name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(birth);
        dest.writeString(company);
        dest.writeString(email);
        dest.writeString(phoneCel);
        dest.writeString(phoneRes);
        dest.writeString(phoneCom);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    public static final Creator<ContactModel> CREATOR = new Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel source) {
            return new ContactModel(source);
        }

        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };
}
